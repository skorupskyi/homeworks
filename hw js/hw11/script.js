let num = prompt("Enter num");
let innerTextArr = [];

for (let i = 0; i < num; i++) {
  innerTextArr[i] = prompt("Enter text");
}

let elemsArr = innerTextArr.map( function(text) {
  let elem = document.createElement('li');
  elem.innerText = `${text}`;

  return elem;
})

let list = document.getElementsByTagName('ul');

for (let elem of elemsArr) {
  list[0].appendChild(elem);
}

let timer = document.createElement('span');
timer.innerText = '10';
document.body.appendChild(timer);
let i = 9;

setInterval( function() {
  timer.innerText = i;
  i--;
},1000);

setTimeout( function() {
  list[0].remove();
  timer.remove()
},10000);