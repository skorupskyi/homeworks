let button = document.getElementById('btn')
let circle = document.createElement('div');

let inputDiametr = document.createElement('input');
inputDiametr.placeholder = 'diametr(px)';

let inputColor = document.createElement('input')
inputColor.placeholder = 'color(rgb,hex,hsl,text)';

let buttonTwo = document.createElement('button')
buttonTwo.innerText = 'draw';
buttonTwo.style.display = 'block';

button.addEventListener('click', ()=> {
  button.remove();
  document.body.appendChild(inputDiametr);
  document.body.appendChild(inputColor);
  document.body.appendChild(buttonTwo);
})

buttonTwo.addEventListener('click', ()=> {
  circle.remove();
  circle.style = `width: ${inputDiametr.value}px; height: ${inputDiametr.value}px;
                  background: ${inputColor.value}; border-radius: 50%`;
  document.body.appendChild(circle);
})