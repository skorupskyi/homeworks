let time = new Date(0,0,0,0,0,0,0);

let timer = document.getElementById('timer');
let pause = document.createElement('button');
let start = document.createElement('button');
let clear = document.createElement('button');

clear.innerText = 'clear';
start.innerText = 'start';
pause.innerText = 'pause';
timer.innerText = time.getMinutes() + ':' + time.getSeconds() + ':' + time.getMilliseconds();

document.body.appendChild(start);
document.body.appendChild(clear);
   
start.addEventListener('click', () => {
  start.remove();
  document.body.insertBefore(pause,clear);
      
  let int = setInterval(() => {
    time.setTime(time.getTime() + 10);
    timer.innerText = time.getMinutes() + ':' + time.getSeconds() + ':' + Math.floor(time.getMilliseconds()/10);
        
    pause.addEventListener('click', () => {
      pause.remove();
      document.body.insertBefore(start,clear);
      clearInterval(int);
    })

  },10)

})

clear.addEventListener('click', ()=> {
  time = new Date(0,0,0,0,0,0,0);
  timer.innerText = time.getMinutes() + ':' + time.getSeconds() + ':' + time.getMilliseconds();
})